
-- a Haskell solution to Schroeder's Problem 2.7 (to run: "stack calc.hs")

microstates 1 q = [[q]] -- only one way to put "q" excitations into 1 oscillator
microstates n q = [(aq:ms) | aq <- [0..q],  ms <- (microstates (n-1) (q-aq))]

join (x:xs) = if length xs > 0 then x ++ ['|'] ++ join xs else x
bars_and_dots amicrostate = join (map (\n -> ['*' | _ <- [1..n]]) amicrostate)

main :: IO()
main = do
  mapM_ putStrLn (map bars_and_dots (microstates 4 2))